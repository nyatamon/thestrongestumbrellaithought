class PGraphicsManager {

  public static final int P_SIZR = 8;

  PGraphic[] pg;

  ArrayList<PGraphic> pgList;

  PGraphicsManager(String dirName) {
    pg = new PGraphic[P_SIZR];
    pgList = new ArrayList<PGraphic>();

    File directory = new File(dataPath("image/"+dirName));
    String[] fileArray = directory.list();
    for (int i = 0; i < fileArray.length; i++) {
      if (!fileArray[i].equals(".DS_Store")) {
        pgList.add(new PGraphic("image/"+dirName+"/"+fileArray[i]));
      }
    }
  }
  
  void disp() {
    for (int i = 0; i < pgList.size(); i++) {
      pushMatrix();
      translate(width/2, height/2);
      rotate(TWO_PI/P_SIZR*(i));
      pgList.get(i).disp();
      popMatrix();
    }           
  }
}