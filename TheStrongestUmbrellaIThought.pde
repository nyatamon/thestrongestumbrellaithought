
public static final int P_MANAGER_SIZR = 8;
PGraphicsManager[] pGraphicsManager;
int dispNum = 0;

String[] imageDirName = {"weather","cafe","test","weather","weather","weather","weather","weather"};

void setup() {
  size(800, 800);
  pGraphicsManager = new PGraphicsManager[P_MANAGER_SIZR];

  for (int i = 0; i < P_MANAGER_SIZR; i++) {
    pGraphicsManager[i] = new PGraphicsManager(imageDirName[i]);
  }
}

void draw() {
  background(255);  
  axis();  
  pGraphicsManager[dispNum].disp();
}


void axis () {
  strokeWeight(1);
  stroke(200);
  line(width/2, 0, width/2, height);
  line(0, height/2, width, height/2);
}

void keyPressed() {
  
  switch (key) {
  case '1':
    dispNum = 0;
    break;
  case '2':
    dispNum = 1;
    break;
  case '3':
    dispNum = 2;
    break;
  case '4':
    dispNum = 3;
    break;
  case '5':
    dispNum = 4;
    break;
  case '6':
    dispNum = 5;
    break;
  case '7':
    dispNum = 6;
    break;
  case '8':
    dispNum = 7;
    break;
  }
}