class PGraphic {
  public static final int P_WIDTH = 370;
  public static final int P_HEIGHT = 306;

  PGraphics pg;
  PImage pi;
  PGraphic(String path) {
    pg = new PGraphics();
    pi = new PImage();
    pi = loadImage(path);
    pg = createGraphics(P_WIDTH, P_HEIGHT);
    pg.beginDraw();
    pg.background(0, 0);
    pg.fill(180);      
    pg.noStroke();
    pg.triangle(0, P_HEIGHT/2, P_WIDTH, 0, P_WIDTH, P_HEIGHT);      
    pg.pushMatrix();
    pg.translate(P_WIDTH/2, P_HEIGHT/2);            
    pg.rotate(PI+HALF_PI);
    pg.fill(255, 0, 0);      
    pg.image(pi, -pi.width/2, -pi.height/2);    
    pg.popMatrix();                 
    pg.endDraw();
  }

  void disp() {
    
    pushMatrix();
    translate(0, -P_HEIGHT/2);         
    image(pg, 0, 0);
    popMatrix();
    
  }
}